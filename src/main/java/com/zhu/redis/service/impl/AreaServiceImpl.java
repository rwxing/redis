package com.zhu.redis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.zhu.redis.dao.AreaDao;
import com.zhu.redis.entity.Area;
import com.zhu.redis.service.AreaService;
@Service
public class AreaServiceImpl implements AreaService {
	@Autowired
	private AreaDao areaDao;
    
	//@Cacheable(value="common") //���뻺��
	@CacheEvict(value="common") //�������
	@Override
	public List<Area> getAreaList() {
		return areaDao.queryArea();
	}

}
